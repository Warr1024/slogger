'use strict';

const log = require('./log');
const objhash = require('./objhash');
const sqldb = require('./sqldb');
const now = require('./now');
const retryloop = require('./retryloop');

const dumpconv = async (app, channel) => {
	let cursor;
	while (true) {
		const resp = await retryloop(async () => await app.client.conversations.history({
			include_all_metadata: true,
			channel,
			limit: 200,
			cursor
		}));
		log({
			channel,
			msgs: resp.messages.length,
			done: !resp.has_more
		});
		for (const msg of resp.messages)
			await sqldb.savenew('conv_history', {
				channel,
				recv_time: now(),
				ts: msg.ts,
				raw: JSON.stringify(msg),
				hash: objhash(msg),
			});
		if (!resp.has_more)
			break;
		cursor = resp.response_metadata.next_cursor;
	}
};

module.exports = async app => {
	const resp = await retryloop(async () => await app.client.conversations.list({
		types: 'public_channel,private_channel,mpim,im'
	}));
	const dumpable = resp.channels.filter(x => x.is_member);
	log({
		all_channels: resp.channels.length,
		dump_channels: dumpable.length
	});
	for (const conv of dumpable) {
		await sqldb.savenew('conv_meta', {
			channel: conv.id,
			recv_time: now(),
			raw: JSON.stringify(conv),
			hash: objhash(conv)
		});
		await dumpconv(app, conv.id);
	}
};