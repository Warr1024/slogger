'use strict';

const fs = require('fs');
const path = require('path');
const sqldb = require('./sqldb');
const log = require('./log');
const objnav = require('./objnav');
const now = require('./now');

const receivableEvents = fs.readFileSync(path.join(__dirname, 'events.txt'))
	.toString()
	.split(/\s+/)
	.map(x => x.trim())
	.filter(x => x);

const getid = (...args) => {
	const res = objnav(...args);
	return res && res.id || res;
};

const recv = async (event_type, data) => {
	const recv_time = now();
	const row = {
		recv_time,
		event_type
	};
	row.body = JSON.stringify(data.body);
	row.subtype = getid(data, 'event', 'subtype');
	row.team_id = getid(data, 'body', 'team_id');
	row.event_ts = getid(data, 'event', 'event_ts');
	row.channel = getid(data, 'event', 'channel') ||
		getid(data, 'event', 'item', 'channel');
	row.channel_type = getid(data, 'event', 'channel_type');
	row.ts = getid(data, 'event', 'ts');
	row.thread_ts = getid(data, 'event', 'thread_ts');
	row.user = getid(data, 'event', 'user') ||
		getid(data, 'event', 'actor_id');
	await sqldb.savenew('event', row);
	log(Object.assign({}, row, {
		body: undefined
	}));
};

module.exports = app => {
	for (const evt of receivableEvents)
		app.event(evt, (...args) => recv(evt, ...args));
	log({
		hooked: receivableEvents.length
	});
};