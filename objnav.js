'use strict';

const objnav = (obj, k, ...sub) => obj ? k ? objnav(obj[k], ...sub) : obj : obj;

module.exports = objnav;
