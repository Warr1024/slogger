'use strict';

const censor = '********';
const redacted = {};
const log = msg => console.log(Object.values(redacted).reduce((a, b) => a.replace(b, censor),
	JSON.stringify([new Date().toISOString(), msg])));
log.redact = x => x && (redacted[x] = new RegExp(x, 'g'));

module.exports = log;