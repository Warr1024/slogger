'use strict';

const log = require('./log');
const objhash = require('./objhash');
const sqldb = require('./sqldb');
const now = require('./now');
const retryloop = require('./retryloop');

module.exports = async app => {
	let page = 1;
	while (true) {
		const resp = await retryloop(async () => await app.client.files.list({
			page
		}));
		log({
			files: resp.files.length,
			page,
			pages: resp.paging.pages,
			more: resp.paging.pages > page
		});
		for (const file of resp.files) {
			await sqldb.savenew('file_meta', {
				id: file.id,
				recv_time: now(),
				raw: JSON.stringify(file),
				hash: objhash(file)
			});
			await sqldb.savenew('file_url', {
				url: file.url_private
			});
		}
		if (resp.paging.pages <= page)
			break;
		page++;
	}
};