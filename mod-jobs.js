'use strict';

const util = require('util');
const config = require('./config');
const log = require('./log');
const sleep = require('./sleep');

const namefunc = (jobName, func) => Object.assign(func, {
	jobName: `${jobName && jobName.jobName || jobName || func}`
});

const jobwrap = job => namefunc(job, async (...args) => {
	log({
		jobName: job.jobName,
		start: true
	});
	try {
		await job(...args);
		log({
			jobName: job.jobName,
			success: true
		});
	} catch (err) {
		log({
			jobName: job.jobName,
			err: util.inspect(err)
		});
		throw err;
	}
});

const jobload = jobName => jobwrap(namefunc(jobName, require(`./job-${jobName}`)));

const jobseries = (...series) => namefunc(JSON.stringify({
		series
	}),
	async (...args) => {
		for (const job of series) await job(...args);
	});

const jobparallel = (...parallel) => namefunc(JSON.stringify({
	parallel
}), async (...args) =>
	await Promise.all(parallel.map(async job => await job(...args))));

const jobcatch = job => namefunc(job, async (...args) => {
	try {
		await job(...args);
	} catch (err) {}
});

const jobtimer = job => (...args) => {
	const run = async () => {
		try {
			await job(...args);
		} finally {
			sleep(config.jobinterval).then(run);
		}
	};
	run();
};

const jobtree = [
	jobtimer(jobseries(
		jobcatch(jobload('joinall')),
		jobparallel(
			jobcatch(jobload('history')),
			jobseries(
				jobcatch(jobload('files')),
				jobcatch(jobload('download'))
			)
		)
	))
];

module.exports = app => jobtree.forEach(job => job(app));