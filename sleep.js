'use strict';

const config = require('./config');

const boxmuller = () => Math.sqrt(-2 * Math.log(Math.random())) *
	Math.cos(2 * Math.PI * Math.random());

module.exports = async t => await new Promise(r => setTimeout(r,
	t * (1 + config.timingjitter * boxmuller())));