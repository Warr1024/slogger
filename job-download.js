'use strict';

const crypto = require('crypto');
const path = require('path');
const fsx = require('fs-extra');
const needle = require('needle');
const wfa = require('write-file-atomic');
const log = require('./log');
const objhash = require('./objhash');
const sqldb = require('./sqldb');
const config = require('./config');
const retryloop = require('./retryloop');

module.exports = async () => {
	while (true) {
		const todo = await sqldb(async db => await db('file_url')
			.whereNull('hash').limit(10));
		if (!todo.length) return;
		for (const row of todo) {
			log({
				download: row.url
			});
			const resp = await retryloop(async () => {
				const raw = await needle('get', row.url, {
					headers: {
						Authorization: `Bearer ${config.bottoken}`
					},
					parse_response: false,
					decode_response: false
				});
				if (raw.statusCode !== 200)
					throw raw.statusCode;
				return raw;
			}, {
				timeout: 600000
			});
			const hash = objhash(crypto.createHash('sha256').update(resp.body).digest('base64'));
			log({
				download: row.url,
				hash,
				size: resp.body.length
			});

			const filedir = path.join(config.datapath, 'files');
			const filefull = path.join(filedir, hash);
			if (!await fsx.pathExists(filefull)) {
				await fsx.ensureDir(filedir);
				await wfa(filefull, resp.body);
			}

			await sqldb(async db => await db('file_url').where({
				url: row.url
			}).update({
				hash
			}));
		}
	}
};