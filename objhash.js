'use strict';

const crypto = require('crypto');

const stripblockids = obj => obj.hasOwnProperty('block_id') ?
	Object.assign({}, obj, { block_id: !!obj.block_id }) :
	obj;

const canonize = obj =>
	!obj ? obj :
	Array.isArray(obj) ? [0, ...obj.map(canonize)] :
	(typeof obj === 'object' && `${obj}` === '[object Object]') ? [1,
		...Object.entries(stripblockids(obj))
			.map(x => [x[0], canonize(x[1])])
			.sort((a, b) => a[0].localeCompare(b[0]))
	] :
	obj;

const alpha = 'abcdefghijklmnopqrstuvwxyz0123456789';
const alphamask = (() => {
	for (let i = 1;; i *= 2)
		if (i >= alpha.length)
			return i - 1;
})();
const targetbits = 128;
const targetlength = Math.ceil(targetbits / Math.log2(alpha.length));

const myhash = data => {
	let seq = 0;
	let str = '';
	while (true)
		for (const raw of crypto.createHash('sha256')
				.update(JSON.stringify([seq, data]))
				.digest()) {
			const masked = raw & alphamask;
			if (masked < alpha.length)
				str += alpha[masked];
			if (str.length >= targetlength)
				return str.substring(0, targetlength);
		}
};

module.exports = obj => myhash(canonize(obj));
