'use strict';

const path = require('path');
const minimist = require('minimist');
const log = require('./log');

const defaults = {
	app: 'slogger',
	sqlfile: 'data.sqlite',
	timeout: 10000,
	jobinterval: 900000,
	timingjitter: 0.1
};
defaults.datapath = defaults.datapath || path.join(process.env.HOME, '.' + defaults.app);

const prefix = `${defaults.app}_`;
for (const [k, v] of Object.entries(process.env))
	if (k.startsWith(prefix))
		defaults[k.substring(prefix.length)] = v;

const config = minimist(process.argv.slice(2), {
	default: defaults
});

log.redact(config.bottoken);
log.redact(config.apptoken);
log({
	config
});

module.exports = config;
