# Slogger

### Brute-Force Slack Logger via Slack Bot

## Features:

- Event Capture
  - Connects to Slack in Socket Mode
  - Listens for every possible known event
  - Records all received events
    - May contain some change data which Slack would otherwise provide only latest-state
- Snapshot Capture
  - Capture all messages in all public channels and conversations the bot is in
  - Capture all shared files visible to the bot
  - Captures periodic full snapshots, keeps history
    - Database contains past snapshot record states (use a windowing query to filter)
    - Only writes unique records (does not add duplicates)
  - Downloads files and stores deduplicated
- Auto-joins all public channels
  - Can be invited to non-public channels to capture them
- Data stored in a SQLite database
- All data is stored with raw JSON blobs included, for later analysis

## Major Missing Things:

- Metadata snapshots
  - Team roster
  - Channel rosters
  - Team metadata
  - Custom emoji

## Setup

Currently this is intended for a "Slack App Developer" audience, and
setting it up involves creating a custom Slack App for each workspace
you want to capture.

- You need to be a workspace admin for the workspace you want to capture.
- Log into the Slack admin web UI
  - In Slack -> workspace menu -> "Settings and Administration" -> "Manage Apps"
- Top right of the admin web UI, open the "Build" link
- "Create New App" button
- Choose "From Scratch"
- Pick a name, choose the workspace you want to capture, then "Create App" button
- Left sidebar -> "OAuth & Permissions"
- Under "Scopes", under "Bot Token Scopes", add:
  - Every scope involving {read, history, list, join} across everything
  - *Except* team.billing.read
- Install to Workspace (at the top)
  - *SAVE* the Bot Token now; this will be the --bottoken= command line param for slogger
- Open "Socket Mode" tab in left sidebar
- Flip the switch to turn it on
- Give it any name and add all scopes
- SAVE the app token; this will be the --apptoken= command line param for slogger
- Click "Event Subscriptions" below
- "Enable Events" -> ON
- Expand "Subscribe to Bot Events"
- Open pulldown, add every event available
  - Everything non-bold should be blue
- At far bottom right of page, "Save Changes"
- If yellow banner at page top tells you to reinstall the app, do it and follow instructions
- You can now customize the bot in Basic Info (give it an icon, change colors)

Once these this setup is complete, run the bot via `node .` with the --bottoken=... and --apptoken=... values from the setup process.

You will probably want to set this up on a server to stay running constantly, e.g. with Docker or systemd or any other supervision system.

Outbound internet connectivity is needed, but you do NOT need to open any listening ports.