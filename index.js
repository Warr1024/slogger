'use strict';

process.on('unhandledRejection', err => { throw err; });

const bolt = require('@slack/bolt');
const config = require('./config');

if (config.hashfix)
	require('./hashfix');

const app = new bolt.App({
	token: config.bottoken,
	appToken: config.apptoken,
	socketMode: true
});

for (const mod of 'eventlogger timeout jobs'.split(' '))
	require(`./mod-${mod}`)(app);

app.start();
