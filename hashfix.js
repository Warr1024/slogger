'use strict';

const sqldb = require('./sqldb');
const objhash = require('./objhash');

const fixtable = async (tbl, ...uxcols) => {
	const uxcolidx = Object.assign(...uxcols.map(k => ({ [k]: true })));
	console.log(`loading ${tbl}...`);
	const rows = await sqldb(async db => await db(tbl)
		.orderBy('recv_time')
		.select());
	console.log(`${tbl}: ${rows.length}`);
	let clean = 0;
	let dirty = 0;
	const rpt = () => console.log(`${clean} clean, ${dirty} dirty, ${Math.floor((clean + dirty) / rows.length * 10000) / 100}%`);
	const interval = setInterval(rpt, 5000);
	try {
		let batch = [];
		const batchflush = async () => await sqldb(async db => {
			for (const b of batch)
				await b(db);
			batch = [];
		});
		for (const row of rows) {
			const nhash = objhash(JSON.parse(row.raw));
			if (nhash === row.hash) { clean++; continue; }
			dirty++;
			batch.push(async db => {
				await db(tbl)
					.where(Object.assign({}, ...Object.entries(row)
						.filter(([k]) => uxcolidx[k])
						.map(([k, v]) => ({ [k]: v }))))
					.delete();
				await db(tbl)
					.insert(Object.assign({}, row, { hash: nhash }))
					.onConflict()
					.ignore();
			});
			if (batch.length >= 1000)
				await batchflush();
		}
		await batchflush();
	} finally {
		clearInterval(interval);
		rpt();
	}
};

(async () => {
	await sqldb(async db => db.raw('pragma quick_check'));
	await fixtable('conv_meta', 'channel', 'hash');
	await fixtable('conv_history', 'channel', 'ts', 'hash');
	await fixtable('file_meta', 'id', 'hash');
	console.log('hashfix complete');
})()
	.catch(err => { throw err; });
