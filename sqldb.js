'use strict';

const path = require('path');
const fsx = require('fs-extra');
const knex = require('knex');
const config = require('./config');

const schema = {
	event: {
		recv_time: 'integer',
		event_type: 'text',
		body: 'text',
		subtype: 'text',
		event_ts: 'text',
		team_id: 'text',
		channel: 'text',
		channel_type: 'text',
		user: 'text',
		ts: 'text',
		thread_ts: 'text',
	},
	conv_meta: {
		channel: 'text',
		recv_time: 'integer',
		raw: 'text',
		hash: 'text',
		replaced: 'integer',
	},
	conv_history: {
		channel: 'text',
		recv_time: 'integer',
		ts: 'text',
		raw: 'text',
		hash: 'text',
	},
	file_meta: {
		id: 'text',
		recv_time: 'integer',
		raw: 'text',
		hash: 'text',
	},
	file_url: {
		url: 'text',
		hash: 'text',
	},
};

const unique = [
	['conv_meta', 'channel', 'hash'],
	['conv_history', 'channel', 'ts', 'hash'],
	['file_meta', 'id', 'hash'],
	['file_url', 'url'],
];

const index = [
	['file_url', 'hash']
];

const ignoreexist = e => {
	if (!/already\s+exist/.test(e) &&
		!/duplicate\s+column\s+name/.test(e)) throw e;
};

const initdb = (async () => {
	await fsx.ensureDir(config.datapath);

	const filename = path.join(config.datapath, config.sqlfile);
	const db = await knex({
		client: 'sqlite3',
		useNullAsDefault: true,
		connection: {
			filename
		}
	});

	for (const [tname, tdef] of Object.entries(schema)) {
		await db.schema.createTable(tname,
				t => t.specificType('rowid', 'integer not null primary key'))
			.catch(ignoreexist);
		for (const [cname, cdef] of Object.entries(tdef))
			await db.schema.alterTable(tname,
				t => t.specificType(cname, cdef))
			.catch(ignoreexist);
	}
	for (const def of unique)
		await db.schema.alterTable(def.shift(), t => t.unique(def))
		.catch(ignoreexist);
	for (const def of index)
		await db.schema.alterTable(def.shift(), t => t.index(def))
		.catch(ignoreexist);

	return db;
})();

const sqldb = async func => await (await initdb)
	.transaction(async db => await func(db));

sqldb.savenew = async (tbl, row) => await sqldb(async db =>
	await db(tbl).insert(row).onConflict().ignore());
module.exports = sqldb;