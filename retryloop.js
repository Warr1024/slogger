'use strict';

const util = require('util');
const config = require('./config');
const log = require('./log');
const sleep = require('./sleep');

const defaults = {
	timeout: config.timeout,
	retry: 5,
	delay: 10000
};

module.exports = async (task, opts) => {
	opts = Object.assign({}, defaults, opts || {});
	for (let attempt = 1; attempt <= opts.retry; attempt++)
		try {
			return await new Promise((res, rej) => {
				const timeout = setTimeout(() => rej('timeout'), opts.timeout);
				(async () => await task())()
				.then(x => (clearTimeout(timeout), res(x)))
					.catch(x => (clearTimeout(timeout), rej(x)));
			});
		}
	catch (err) {
		log(Object.assign({}, opts, {
			err: util.inspect(err),
			attempt
		}));
		if (attempt >= opts.retry)
			throw err;
		await sleep(opts.delay);
	}
};