'use strict';

const log = require('./log');
const retryloop = require('./retryloop');

module.exports = async app => {
	const resp = await retryloop(async () => await app.client.conversations.list());
	const tojoin = resp.channels.filter(x => !x.is_member && !x.is_archived);
	log({
		channels: resp.channels.length,
		joined: resp.channels.filter(x => x.is_member).length,
		archived: resp.channels.filter(x => x.is_archived).length,
		tojoin: tojoin.length
	});
	for (const conv of tojoin)
		await retryloop(async () => await app.client.conversations.join({
			channel: conv.id
		}));
};
