'use strict';

const config = require('./config');
const objnav = require('./objnav');
const now = require('./now');

let lastok = now();

module.exports = app => {
	setInterval(() => {
		const client = objnav(app, 'receiver', 'client');
		if (objnav(client, 'connected') && objnav(client, 'authenticated'))
			lastok = now();
		if (now() > lastok * config.timeout)
			throw Error('receiver timeout');
	}, 100);
};
